const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

module.exports = {
  entry: {
    main: [
      './src/utils/polyfills.js',
      './src/main.js',
      './src/utils/google-analytics.js',
      '/src/style/main.scss'
    ],
    init: ['./src/js/init.js', './src/style/init.scss'],
    wycena: ['reusable-webc/dist/radio.js', './src/js/view-wycena.js'],
    kontakt: ['./src/js/view-contact.js'],
    'o-mnie': ['./src/style/about.scss'],
    referencje: ['./src/style/reference.scss'],
    courses: [
      './src/js/view-contact.js',
      './src/js/view-wycena.js',
      'reusable-webc/dist/radio.js',
      './src/style/course.scss',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
};
