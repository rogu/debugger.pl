
module.exports = {
    plugins: [
        'postcss-preset-env',
        require('postcss-import'),
        require('tailwindcss'),
        require('postcss-nested'), // or require('postcss-nesting')
        require('autoprefixer'),
    ],
};
