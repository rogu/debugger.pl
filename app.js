require('dotenv').config();
const express = require('express');
const hbs = require('./src/hbs-settings').set(require("express-handlebars"));
const path = require('path');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
require('./src/db');
const app = express();
const PORT = 2222;
const { access } = require('./src/middleware');
const routes = require('./src/routes');
require('console-stamp')(console, 'dd/mm/yyyy HH:MM:ss');

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.set('views', path.join(__dirname, 'src/views'));
app.use(access);
app.use(favicon(path.join(__dirname, 'public', 'assets/icons/favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use('/', routes);
app.set('json spaces', 4);
app.set('Cache-Control', 'public, max-age=31557600');

app.listen(PORT, () => console.log(`server works on ${PORT}`));
