/* import { precacheAndRoute,registerRoute } from 'workbox-precaching';


registerRoute(
    new RegExp('https://jsonplaceholder.typicode.com/users')
); */
import { precacheAndRoute } from 'workbox-precaching';

precacheAndRoute([
    { url: '/', revision: 111 },

    { url: '/szkolenie-angular', revision: 111 },
    { url: '/szkolenie-angular-zaawansowane', revision: 111 },
    { url: '/szkolenie-angular-ngrx', revision: 111 },
    { url: '/szkolenie-programowanie-reaktywne', revision: 111 },
    { url: '/szkolenie-javascript-podstawy', revision: 111 },
    { url: '/szkolenie-javascript-zaawansowane', revision: 111 },
    { url: '/szkolenie-web-components', revision: 111 },
    { url: '/szkolenie-testy-automatyczne-cypress', revision: 111 },
    { url: '/szkolenie-nodejs-i-mongodb', revision: 111 },
    { url: '/szkolenie-nodejs-nestjs', revision: 111 },
    { url: '/szkolenie-system-linux', revision: 111 },
    { url: '/szkolenie-angular-ngrx', revision: 111 },
    { url: '/szkolenie-react-typescript-nextjs', revision: 111 },
    { url: '/szkolenie-tailwind-css', revision: 111 },
    { url: '/o-mnie', revision: 111 },
    { url: '/kontakt', revision: 111 },
    { url: '/wycena', revision: 111 }
]);

// Use with precache injection
precacheAndRoute(self.__WB_MANIFEST);
