const fs = require('fs'),
    path = require('path'),
    useragent = require('useragent');

function getContent(url, splitPattern, size, excludedBrowsers, excludeIp) {
    const splitMarker = '--x--';
    const browsers = [];
    return fs
        .readFileSync(url).toString()
        .replace(splitPattern, value => {
            return splitMarker + value
        })
        .split(splitMarker)
        .reverse()
        .map((value) => {
            const fa = useragent.lookup(value).family;
            if (!browsers.includes(fa)) {
                browsers.push(fa);
            }
            return value;
        })
        .filter((value) => value)
        .filter((value) => {
            if (!excludeIp) return true;
            const idPattern = /(\d+\.){3}\d+/;
            return !(value.match(idPattern)[0] === excludeIp);
        })
        .filter((value) => {
            if (!excludedBrowsers) return true;
            return !(new RegExp(excludedBrowsers, 'i').test(useragent.lookup(value).family));
        })
        .filter(value => {
            let from = value.match(/"GET.+HTTP\/\d+\.\d+"/);
            const isExcluded = new RegExp(['src', 'assets', 'logs', 'favicon', 'libs'].join('|'));
            return !from || !isExcluded.test(from[0]);
        })
        .map((value) => `<p>${value.replace(/" ".*/, (val) => `" ${useragent.lookup(val).family}`)}</p>`)
        .slice(0, size || 50)
        .join('');
}

module.exports = {
    logs(req, res) {
        const pattern = /\d+(.\d+){3} -/g;
        const url = path.join('..', 'logs', 'access.log');
        const result = getContent(url, pattern, req.params.size, req.query.eb, '89.65.78.177');
        res.send(result);
    },

    errors(req, res) {
        const pattern = /\[.+(:\d{2}){2}.*\]/g;
        const url = path.join('..', 'logs', 'error.log');
        const result = getContent(url, pattern, req.params.size, req.query.eb);
        res.send(result);
    }
};