const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
    name: String,
    fields: Array
});

const Course = mongoose.model('course', courseSchema);

const coursesController = {
    fetchCourses() {
        return Course.find().lean();
    }
};

module.exports = coursesController;
