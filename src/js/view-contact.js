import { Validate } from "../utils/validate";
import Utils from "debugger-look/dist/script/utils";
const ui = Utils.uiHandler(
    ['#contactForm', 'submit', (ev) => {
        ev.preventDefault();
        new Validate(ev.target, sendForm);
    }],
    ['.sending']
)

function dots() {
    let i = 0;
    let dir = +1;
    return setInterval(() => {
        dir = (i === 4) && -1 || (i === 0) && +1 || dir;
        i = i + dir;
        ui.sending.innerHTML = '>' + '>'.repeat(i);
    }, 150);
}

const sendForm = async (data) => {
    if(!navigator.onLine) {
        alert('You are offline; try later.');
        return;
    }
    const int = dots();
    ui.sending.classList.toggle('active');
    const { status } = await fetch('msg', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ ...data, page: document.title })
    });
    status === 200
        ? (function () {
            clearInterval(int);
            ui.sending.innerHTML = 'wiadomość wysłana, dziękuję.';
        })()
        : alert(JSON.stringify('server error'));
}
