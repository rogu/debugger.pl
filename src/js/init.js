import 'reusable-webc/dist/wordscloud';

const words = [
  ['konsultacje', 'warsztaty', 'szkolenia', '&nbsp;'],
  ['JavaScript', 'TypeScript', 'Node.js', 'React', 'Angular'],
];
const el = document.querySelector('ui-words-cloud');
el.render
  ? el.render(words)
  : el.addEventListener('ready', () => el.render(words));
