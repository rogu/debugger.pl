import Utils from "debugger-look/dist/script/utils";
import * as Helpers from "../utils/helpers";

const userType = Helpers.getQueryParams().user || 'normal';

const base = 1000;

let initState = {
    participantsCount: 0,
    durationDays: 0,
    farAway: false,
    oneDayCost: {
        flame: base * 1.3,
        spark: base * 1.7,
        normal: base * 2
    }[userType],
    hotelCostPerDay: 150,
    travelCost: 300,
    userRate: 200
};

const ui = Utils.uiHandler(
    ['#result'], ['#travelBox'], ['#online'], ['#onlineCost'], ['#hotel'], ['#travel'],
    ['#participantsCount', 'CHANGED', updateView],
    ['#durationDays', 'CHANGED', updateView],
    ['#farAway', 'change', ({ target: { value } }) => updateView({ detail: { farAway: value } })]
)

function calc({ participantsCount, durationDays, farAway, oneDayCost, hotelCostPerDay, travelCost, userRate }) {
    let hotelCost = hotelCostPerDay * durationDays;
    let userCost = userRate * durationDays;
    let totalUsersCost = userCost * participantsCount;
    let primaryTotalCost = oneDayCost * durationDays;

    let onlineCost = 55 * participantsCount;

    let extras = farAway === 'stacjonarne' ? hotelCost + travelCost : onlineCost;
    return {
        totalCost: primaryTotalCost + totalUsersCost + extras,
        hotelCost,
        travelCost,
        onlineCost
    };
}

function updateView({ detail }) {
    initState = Object.assign({}, initState, detail);
    if (initState.participantsCount && initState.durationDays && initState.farAway) {
        const state = calc(initState);
        ui.result.innerHTML = initState.farAway === 'stacjonarne'
            ? `<s>${state.totalCost}</s> ${Math.floor(state.totalCost * .95)}`
            : state.totalCost;
        ui.hotel.innerHTML = state.hotelCost;
        ui.travel.innerHTML = state.travelCost;
        ui.onlineCost.innerHTML = state.onlineCost;
        ui.online.style.display = initState.farAway === 'zdalne' ? 'block' : 'none';
        ui.travelBox.style.display = initState.farAway === 'stacjonarne' ? 'block' : 'none';
        if (userType !== 'normal') {
            ui.travelBox.querySelector('div').removeAttribute('hidden');
            ui.online.querySelector('div').removeAttribute('hidden');
        }
    }
    return detail;
}
