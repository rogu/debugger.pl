// todo: watch move to index

import "debugger-look/dist/script/menu";
import "debugger-look/dist/script/utils";
import 'tw-elements/dist/src/js/bs/dist/collapse';

const { Workbox } = require("workbox-window");

if (process.env.NODE_ENV !== "production") {
  console.log("Looks like we are in development mode!");
}

if ("serviceWorker" in navigator) {
  /* navigator.serviceWorker.register('/sw.js').then(function () {
        console.log("Service Worker Registered");
    }); */
  const wb = new Workbox("/sw.js");

  wb.register();

  wb.addEventListener("activate", function (event) {
    console.log("-------activate", event);
    event.waitUntil(
      caches.keys().then((cacheNames) => {
        cacheNames.forEach((cacheName) => {
          caches.delete(cacheName);
        });
      })
    );
    caches.keys().then((cacheNames) => {
      cacheNames.forEach((cacheName) => {
        caches.delete(cacheName);
      });
    });
  });

  wb.addEventListener("waiting", (ev) => {
    console.log("-------waiting", ev);
  });

  wb.addEventListener("message", (ev) => {
    console.log("-------message", ev);
  });

  wb.addEventListener("installed", (ev) => {
    console.log("-------installed", ev);
  });

  wb.addEventListener("push", (ev) => {
    console.log("-------push", ev);
  });
  wb.update = () => {
    console.log("up");
  };
}
