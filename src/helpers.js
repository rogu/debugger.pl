const nodemailer = require('nodemailer');
const mailConfig = {
  host: 'mail14.mydevil.net',
  port: 587,
  secure: false,
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAIL_PASS,
  },
};

module.exports = {
    sendMail(from, to, subject, text) {
        const transporter = nodemailer.createTransport(mailConfig);
        const mailOptions = {
            from: /* from || */ mailConfig.auth.user,
            to: to || mailConfig.auth.user,
            subject,
            text
        };
        return transporter.sendMail(mailOptions);
    },
    msg({ body }, res) {
        this
            .sendMail(body.email,
                null,
                body.page,
                `name: ${body.name}\nemail: ${body.email}\nmessage: ${body.msg}`)
            .then((resp) => res.send({ message: 'success\n' + resp.response, body }))
            .catch((error) => res.status(400).send({ error }));
    },
    get args() {
        const args = process.argv.filter((item) => item.startsWith('-'));
        const argsToObj = args.reduce((acc, arg) => {
            const item = arg.split('=');
            return { ...acc, [item[0].substr(1)]: item[1] }
        }, {})
        return argsToObj;
    },
    mapToEntities(arr, key) {
        return arr.reduce((acc, item) => {
            return { ...acc, [item[key]]: item }
        }, {})
    }
};
