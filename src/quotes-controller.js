const mongoose = require('mongoose');

const quoteSchema = new mongoose.Schema({
    author: String,
    quote: String
});

const Quote = mongoose.model('quotes', quoteSchema);

const quotesController = {
    fetch() {
        return Quote.find().lean();
    }
};

module.exports = quotesController;
