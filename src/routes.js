const express = require('express');
const helpers = require('./helpers');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const logs = require('./logs');
const { emailOk } = require('./middleware');
const coursesController = require('./courses-controller');
const quotesController = require('./quotes-controller');
const { version } = require('../package.json');

function getClass(page) {
  switch (true) {
    case page === 'o-mnie':
      return 'bg-about';
    case page === 'init':
      return 'bg-init';
    case page === 'kontakt':
      return 'bg-net';
  }
}

function getName(group, page) {
  switch (true) {
    case group === 'courses':
      return 'szkolenie-' + page;
    case page === 'init':
      return '';
    default:
      return page;
  }
}

const renderPage = (sections, section, page, req, res) => {
  let options = {
    title: getName(section, page.name),
    name: page.name,
    class: getClass(page.name),
    sections,
    data: page.data,
    logo: fs.readFileSync('./public/assets/vectors/logo.svg'),
    hamburger: fs.readFileSync('./public/assets/vectors/hamburger.svg'),
    home: fs.readFileSync('./public/assets/vectors/home.svg'),
    version,
  };
  const fields = page.fields && helpers.mapToEntities(page.fields, 'label');
  switch (true) {
    case page.name === 'init':
      res.render(path.join('pages', section, page.name), {
        ...options,
        scripts: ['init'],
        css: ['init'],
      });
      break;
    case section === 'courses':
      res.render(path.join('partials', 'course'), {
        ...options,
        program: fields.program.value,
        extraTitle: fields.extraTitle.value,
        demo: fields.demo.value,
        purpose: fields.purpose.value,
        recipient: fields.recipient.value,
        description: fields.description.value,
        duration: fields.duration.value,
        ...(fields.about && { about: fields.about.value }),
        scripts: ['courses'],
        css: ['courses'],
      });
      break;
    case page.name === 'wycena':
      res.render(path.join('pages', 'about', 'wycena'), {
        ...options,
        scripts: ['wycena'],
      });
      break;
    case page.name === 'kontakt':
      res.render(path.join('pages', 'about', 'kontakt'), {
        ...options,
        scripts: ['kontakt'],
      });
      break;
    case page.name === 'referencje':
      res.render(path.join('pages', 'about', 'referencje'), {
        ...options,
        css: ['referencje'],
      });
      break;
    case page.name === 'o-mnie':
      res.render(path.join('pages', 'about', 'o-mnie'), {
        ...options,
        css: ['o-mnie'],
        extraTitle:
          'Robert&nbsp;Gurgul Trener: Angular, React, JavaScript, Node.js, Cypress',
      });
      break;
    default:
      res.render(path.join('pages', section, page.name), options);
  }
};

function getData(page) {
  const sortArr = [
    'alior',
    'com',
    'ass',
    'eni',
    'ing',
    'get',
    '180',
    'sof',
    'win',
    'tra',
    'jav',
    'dsa',
    'bit',
    'bit',
    'uj',
  ];
  switch (page) {
    case 'referencje':
      return Promise.resolve(
        fs.readdirSync('public/assets/reco/jpg').sort(function (a, b) {
          let a2 = sortArr.find((data) => a.startsWith(data));
          let b2 = sortArr.find((data) => b.startsWith(data));
          return sortArr.indexOf(a2) > sortArr.indexOf(b2) ? 1 : -1;
        })
      );
    case 'quotes':
      return quotesController.fetch();
  }
}

function getStaticPages(link, value) {
  return Promise.all(
    fs.readdirSync(path.join(link, value)).map(async (file) => {
      const name = file.replace('.handlebars', '');
      return { name, data: await getData(name) };
    })
  );
}

async function createRoutes() {
  const data = await coursesController.fetchCourses();
  const courses = data.sort((a, b) => (a.name > b.name ? 1 : -1));
  const about = await getStaticPages('src/views/pages', 'about');
  const outside = await getStaticPages('src/views/pages', 'outside');
  let sections = { courses, about, outside };
  for (let section in sections) {
    sections[section].forEach((page) => {
      router.get(
        '/' + getName(section, page.name),
        renderPage.bind(null, sections, section, page)
      );
    });
  }
}

createRoutes();

router.post('/msg', emailOk, helpers.msg.bind(helpers));

router.get('/logs', logs.logs);
router.get('/logs/:size', logs.logs);
router.get('/errors', logs.errors);
router.get('/errors/:size', logs.errors);

module.exports = router;
