export class Validate {

    constructor(form, callback) {
        this.form = form;
        this.data = {};
        this.hasError = false;
        this.checkForm(callback);
    }

    checkForm(callback) {
        for (let field of this.form) {
            if (!field.hasAttribute('name')) continue;
            this.updateData(field);
            if (field.hasAttribute('required')) this.checkField(field);
        }
        if (this.form.checkValidity() && !this.hasError) {
            callback(this.data);
            this.form.reset();
        }
    }

    updateData(field) {
        if (field.type === 'radio') {
            const radio = this.form.querySelector('input[name=' + field.name + ']:checked');
            this.data[field.name] = radio ? radio.value : '';
        } else {
            this.data[field.name] = field.value;
        }
    }

    checkField(field) {
        this.clearError(field);
        switch (field.tagName) {
            case "SELECT":
                !(field.selectedIndex != 0) && this.setError(field);
                break;
            case "TEXTAREA":
                !(field.value && field.value != field.defaultValue) && this.setError(field);
                break;
            case "INPUT":
                switch (field.getAttribute('type')) {
                    case "text":
                        !(field.value && field.value != field.defaultValue) && this.setError(field);
                        break;
                    case "number":
                        !parseInt(field.value) && this.setError(field);
                        break;
                    case "email":
                        var regex = /^\w+([\+.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
                        !regex.test(field.value) && this.setError(field);
                        break;
                    case "radio":
                        var checked = this.form.querySelector('input[name=' + field.name + ']:checked');
                        !checked && this.setError(field);
                        break;
                }
                break;
        }
    }

    clearError(field) {
        const err = field.parentNode.querySelector('small');
        if (err) field.parentNode.removeChild(err);
    }

    setError(field) {
        const err = field.getAttribute('err') || 'Pole wymagane';
        const span = document.createElement('small');
        span.style = `
            color: rgb(220, 38, 38);
            font-size: 1em;
        `;
        span.innerText = err;
        field.parentNode.appendChild(span);
        this.hasError = true;
    }

}
