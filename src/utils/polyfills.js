// edge/ie closest polyfills
if (!Element.prototype.closest) {
    Element.prototype.closest = function (selector) {
        let el = this;
        while (el) {
            if (el.matches(selector)) {
                return el;
            }
            el = el.parentElement;
        }
    }
}
