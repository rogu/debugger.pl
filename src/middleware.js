function emailOk({ body }, res, next) {
    return body.email ? next() : res.status(400).send({ ok: false, info: 'email is required' });
}

function access(req, res, next) {
    if (req.headers.origin) {
        res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    }
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');

    next();
}

module.exports = { emailOk, access };
