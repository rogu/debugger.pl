const EC = protractor.ExpectedConditions;

describe('contact page', function () {

    browser.waitForAngularEnabled(false);
    const drv = browser.driver;

    beforeEach(function () {
        drv.get(browser.baseUrl + "kontakt");
        /*const overlay = drv.findElement(by.css('.overlay'));
        overlay.click();*/
        browser.sleep(1000);
    });

    function checkErrors(num) {
        const errors = drv.findElements(by.css("label > small"));
        errors
            .then(
                function (val) {
                    expect(val.length).toBe(num);
                }
            );
    }

    function checkForm(form, callback) {

        const btnSend = drv.findElement(by.css('.btn-send'));
        btnSend.click();

        const date = new Date();
        const name = 'test day:' + date.getDate() + " minutes:" + date.getMinutes();
        form.findElement(by.name('name')).sendKeys(name);
        form.findElement(by.name('email')).sendKeys('test@test.pl');
        form.findElement(by.name('msg')).sendKeys('test test test...');

        btnSend.click();
        callback && callback();
    }

    it('should show 3 errors', function () {
        drv
            .findElement(by.css('.btn-send'))
            .click();
        checkErrors(3);
    });

    it('should validate all fields', function () {
        const contactForm = drv.findElement(by.id('contactForm'));
        checkForm(contactForm, function () {
            checkErrors(0);
            const send = element(by.css('.sending'));
            browser.wait(EC.textToBePresentInElement(send.getText(), "wiadomość wysłana, dziękuję."), 5000, "Text is not something I've expected");
        });
    });

});
