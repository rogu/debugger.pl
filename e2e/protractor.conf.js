exports.config = {
    allScriptsTimeout: 11000,

    specs: [
        '*spec.js'
    ],

    capabilities: {
        'browserName': 'chrome'
    },

    //baseUrl: 'http://localhost:63342/js-basic/examples/',
    baseUrl: 'https://debugger.pl/',

    framework: 'jasmine',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },
    onPrepare() {
        browser.driver.manage().window().setPosition(300, 0);
        //browser.driver.manage().window().maximize();
    }
};
