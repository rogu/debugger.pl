function Helpers() {

    this.timeOut = 10000;

    this.waitForAlert = function () {
        browser.wait(function () {
            return browser.switchTo().alert().then(
                function (alert) {
                    alert.accept();
                    return true;
                },
                function () {
                    return false;
                }
            );
        }, this.timeOut);
    };

    this.isElementExist = function (el, cb) {
        if (el.size() < 0) {
            cb(el)
        }
    }
}

module.exports = Helpers;