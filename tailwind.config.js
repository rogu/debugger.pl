module.exports = {
  content: [
    './src/views/**/*.handlebars',
    './src/js/*',
    './src/style/**/*',
    './node_modules/tw-elements/dist/js/**/*.js',
  ],
  theme: {
    extend: {
      fontSize: {
        'body-lg': '1rem',
        'body': '.8rem'
      }
    },
    container: {
      center: true,
      screens: {
        sm: '600px',
        md: '728px',
        lg: '984px',
        xl: '1024px',
        '2xl': '1024px',
      },
    },
  },
  plugins: [require('tw-elements/dist/plugin')],
  darkMode: 'class',
};
