var fs = require('fs')
const file = 'public/sw.js';
fs.readFile(file, 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    var result = data.replace(/111/g, Date.now());

    fs.writeFile(file, result, 'utf8', function (err) {
        if (err) return console.log(err);
    });
});
