const date = new Date();
const title = 'test day:' + date.getDate() + " minutes:" + date.getMinutes();

describe('contact form', () => {
    it('should send message', () => {
        cy.visit('https://debugger.pl/kontakt');
        cy.get(':nth-child(1) > label > .form-control').type(title);
        cy.get(':nth-child(3) > label > .form-control').type('cypress@cypress.pl');
        cy.get(':nth-child(5) > label > .form-control').type('test cypress: ' + new Date());
        cy.get('.btn-send').click();
        cy.get('label > small').should('have.length', 0);
        cy.get('.sending').should('have.text', 'wiadomość wysłana, dziękuję.');
    })
})
